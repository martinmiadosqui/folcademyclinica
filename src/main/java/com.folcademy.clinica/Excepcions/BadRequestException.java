package com.folcademy.clinica.Excepcions;

public class BadRequestException extends RuntimeException{
    public BadRequestException(String message) {

        super(message);
    }
}
