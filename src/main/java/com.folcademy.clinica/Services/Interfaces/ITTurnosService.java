package com.folcademy.clinica.Services.Interfaces;

import com.folcademy.clinica.Model.Entities.Turno;

import java.util.List;

public interface ITTurnosService {
    List<Turno> findAllTurnos();
}
