package com.folcademy.clinica.Model.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class MedicoEnteroDto {
    public Integer idMedico;

    public String nombre = "";

    public String apellido = "";

    public String profesion = "";

    public Integer consulta = 0;
}
