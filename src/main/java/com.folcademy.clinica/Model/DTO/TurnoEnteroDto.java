package com.folcademy.clinica.Model.DTO;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class TurnoEnteroDto {
   public Integer idTurno;

   public LocalDate fecha;

   public LocalTime hora;

   public Boolean atendido;

   public Integer idpaciente;

   public Integer idmedico;
}

